import Vue from 'vue'
import VueResource from 'vue-resource'
import VueRouter from 'vue-router'
import App from './App.vue'
import Header from './Header.vue'
// Pages
import Content from './pages/Content.vue'
import Page1 from './pages/Page1.vue'
import Page2 from './pages/Page2.vue'
import Post from './pages/Post.vue'


Vue.use(VueResource)
Vue.use(VueRouter)

Vue.filter('capitalize', function(value) {
    if (!value) return ''
    value = value.toString();
    return value.replace(/\b\w/g, function(l) { return l.toUpperCase() })
});

Vue.component('vue-header', Header);
Vue.component('vue-content', Content);

var router = new VueRouter({
    routes: [
        { path: '/page1', component: Page1 },
        { path: '/page2', component: Page2 },
        { path: '/blog', component: Content },
        { path: '/post/:id', name: 'post', component: Post }
    ]
})

new Vue({
    el: '#app',
    router: router,
    render: h => h(App)
})
